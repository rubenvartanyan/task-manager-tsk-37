package ru.vartanyan.tm.endpoint;

import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.endpoint.EndpointLocator;
import ru.vartanyan.tm.bootstrap.Bootstrap;
import ru.vartanyan.tm.marker.IntegrationCategory;

public class UserEndpointTest {

    final EndpointLocator endpointLocator = new Bootstrap();

    private Session sessionAdmin;

    @Before
    @SneakyThrows
    public void before() {
        sessionAdmin = endpointLocator.getSessionEndpoint().openSession("admin", "admin");
    }

    @After
    @SneakyThrows
    public void after() {
        endpointLocator.getSessionEndpoint().closeSession(sessionAdmin);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeUserByLoginTest() {
        User user1 = new User();
        user1.setLogin("BB");
        endpointLocator.getAdminEndpoint().addUser(user1, sessionAdmin);
        endpointLocator.getUserEndpoint().removeUserByItsLogin("BB", sessionAdmin);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findUserByLogin() {
        User user = new User();
        user.setLogin("AA");
        final String login = user.getLogin();
        endpointLocator.getAdminEndpoint().addUser(user, sessionAdmin);
        final User userFound = endpointLocator.getUserEndpoint().findUserByLogin(login, sessionAdmin);
        //Assert.assertEquals("AA", userFound.getLogin());
        Assert.assertEquals("AA", userFound.getLogin());
    }

}
