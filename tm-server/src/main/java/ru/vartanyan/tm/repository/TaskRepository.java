package ru.vartanyan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.ITaskRepository;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.model.Task;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    public TaskRepository(Connection connection) {
        super(connection);
    }

    @Override
    protected String getTableName() {
        return "APP_TASK";
    }

    @Override
    public void add(@NotNull Task task) throws SQLException {
        @NotNull final String query =
                "INSERT INTO " + getTableName() +
                        "(`id`, `name`, `description`, `user_id`, `created`, " +
                        "`date_started`, `date_finish`, `status`, `project_id`) " +
                        "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, task.getId());
        statement.setString(2, task.getName());
        statement.setString(3, task.getDescription());
        statement.setString(4, task.getUserId());
        statement.setDate(5, convert(task.getCreated()));
        statement.setDate(6, convert(task.getDateStarted()));
        statement.setDate(7, convert(task.getDateFinish()));
        statement.setString(8, task.getStatus().toString());
        statement.setString(9, task.getProjectId());
        statement.execute();
    }

    @Override
    protected Task fetch(ResultSet resultSet) throws SQLException {
        if (resultSet == null) return null;
        @NotNull Task task = new Task();
        task.setId(resultSet.getString("ID"));
        task.setName(resultSet.getString("NAME"));
        task.setProjectId(resultSet.getString("PROJECT_ID"));
        task.setDescription(resultSet.getString("DESCRIPTION"));
        task.setDateStarted(resultSet.getDate("DATE_STARTED"));
        task.setDateFinish(resultSet.getDate("DATE_FINISH"));
        task.setUserId(resultSet.getString("USER_ID"));
        task.setStatus(Status.valueOf(resultSet.getString("STATUS")));
        return task;
    }

    @Override
    @NotNull
    public final List<Task> findAllByProjectId(@NotNull final String projectId,
                                               @NotNull final String userId)
            throws SQLException {
        @NotNull final String query =
                "SELECT * FROM " + getTableName() +
                        " WHERE `user_id` = ? AND `project_id` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        @Nullable ResultSet resultSet = statement.executeQuery();
        @Nullable List<Task> result = new ArrayList<>();
        if (resultSet != null) {
            while (resultSet.next()) result.add(fetch(resultSet));
        }
        statement.close();
        return result;
    }

    @Override
    public void removeAllByProjectId(@NotNull final String projectId,
                                     @NotNull final String userId) throws SQLException {
        @NotNull final String query =
                "DELETE FROM " + getTableName() +
                        " WHERE `user_id` = ? AND `project_id` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        statement.execute();
    }

    @Override
    public final void bindTaskByProjectId(@NotNull final String projectId,
                                          @NotNull final String taskId,
                                          @NotNull final String userId) throws SQLException {
        final Task task = findById(userId, taskId);
        if (task == null) return;
        task.setProjectId(projectId);
        @NotNull final String query =
                "UPDATE " + getTableName() +
                        " SET `project_id` = ?" +
                        " WHERE `user_id` = ? AND `id` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, projectId);
        statement.setString(2, userId);
        statement.setString(3, taskId);
        statement.execute();
    }

    @Override
    public final void unbindTaskFromProject(@NotNull final String projectId,
                                             @NotNull final String taskId,
                                             @NotNull final String userID) throws SQLException {
        @Nullable final Task task = findById(userID, taskId);
        if (task == null) return;
        task.setProjectId(null);
        @NotNull final String query =
                "UPDATE " + getTableName() +
                        " SET `project_id` = NULL" +
                        " WHERE `user_id` = ? AND `id` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userID);
        statement.setString(2, taskId);
        statement.execute();
    }

}
