package ru.vartanyan.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.api.repository.IProjectRepository;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.model.Project;

import java.sql.*;

public class ProjectRepository extends AbstractBusinessRepository<Project> implements IProjectRepository {

    public ProjectRepository(Connection connection) {
        super(connection);
    }

    @Override
    protected String getTableName() {
        return "APP_PROJECT";
    }

    @Override
    public void add(@NotNull Project project) throws SQLException {
        @NotNull final String query =
                "INSERT INTO " + getTableName() +
                        "(`id`, `name`, `description`, `user_id`, `created`, " +
                        "`date_started`, `date_finish`, `status`) " +
                        "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, project.getId());
        statement.setString(2, project.getName());
        statement.setString(3, project.getDescription());
        statement.setString(4, project.getUserId());
        statement.setDate(5, convert(project.getCreated()));
        statement.setDate(6, convert(project.getDateStarted()));
        statement.setDate(7, convert(project.getDateFinish()));
        statement.setString(8, project.getStatus().toString());
        statement.execute();
    }

    @Override
    protected Project fetch(ResultSet resultSet) throws SQLException {
        if (resultSet == null) return null;
        @NotNull Project project = new Project();
        project.setId(resultSet.getString("ID"));
        project.setName(resultSet.getString("NAME"));
        project.setDescription(resultSet.getString("DESCRIPTION"));
        project.setDateStarted(resultSet.getDate("DATE_STARTED"));
        project.setDateFinish(resultSet.getDate("DATE_FINISH"));
        project.setUserId(resultSet.getString("USER_ID"));
        project.setStatus(Status.valueOf(resultSet.getString("STATUS")));
        return project;
    }

}
