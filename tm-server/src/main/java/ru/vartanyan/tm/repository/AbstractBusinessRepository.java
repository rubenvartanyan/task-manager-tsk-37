package ru.vartanyan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IBusinessRepository;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.model.AbstractBusinessEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity>
        extends AbstractRepository<E> implements IBusinessRepository<E> {

    public AbstractBusinessRepository(Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public final List<E> findAll(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE `user_id` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        @NotNull ResultSet resultSet = statement.executeQuery();
        @NotNull List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Nullable
    @Override
    public final E findById(@NotNull final String id,
                      @NotNull final String userId) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() +
                " WHERE `id` = ? " +
                "AND `user_id` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, id);
        statement.setString(2, userId);
        @NotNull ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable E entity = fetch(resultSet);
        statement.close();
        return entity;
    }

    @Override
    public void clear(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE `user_id` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.execute();
        statement.close();
    }

    @Override
    public void removeById(@NotNull final String id,
                           @NotNull final String userId) throws SQLException {
        @NotNull final String query =
                "DELETE FROM " + getTableName() +
                        " WHERE `id` = ?" +
                        " AND `user_id` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, id);
        statement.setString(2, userId);
        statement.execute();
        statement.close();
    }

    @Nullable
    @Override
    public final E findOneByIndex(@NotNull final Integer index,
                            @NotNull final String userId) throws SQLException {
        @NotNull final String query =
                "SELECT * FROM " + getTableName() +
                        " WHERE `user_id` = ?" +
                        " LIMIT ?, 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setInt(2, index);
        @NotNull ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable E entity = fetch(resultSet);
        statement.close();
        return entity;
    }

    @Nullable
    @Override
    public final E findOneByName(@NotNull final String name,
                           @NotNull final String userId) throws SQLException {
        @NotNull final String query =
                "SELECT * FROM " + getTableName() +
                        " WHERE `name` = ?" +
                        " AND `user_id` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        @NotNull ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable E entity = fetch(resultSet);
        statement.close();
        return entity;
    }

    @Override
    public void removeOneByIndex(@NotNull final Integer index,
                                 @NotNull final String userId) throws Exception {
        @Nullable final E entity = findOneByIndex(index, userId);
        if (entity == null) throw new NullObjectException();
        removeById(entity.getId());
    }

    @Override
    public void removeOneByName(@NotNull final String name,
                                @NotNull final String userId) throws SQLException {
        @NotNull final String query =
                "DELETE FROM " + getTableName() +
                        " WHERE `name` = ?" +
                        " AND `user_id` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        statement.execute();
        statement.close();
    }

    /*
    @NotNull
    @Override
    public final List<E> findAll(@NotNull final Comparator<E> comparator,
                                          @NotNull final String userId) {
        return entities.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    public void showEntity(@NotNull final E entity) {
        System.out.println("[NAME: " + entity.getName() + " ]");
        System.out.println("[DESCRIPTION: " + entity.getDescription() + " ]");
        System.out.println("STATUS: " + entity.getStatus() + " ]");
    }
     */

    @Override
    public void updateEntityById(String id,
                          String name,
                          String description,
                          String userId) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final String query =
                "UPDATE " + getTableName() +
                        " SET `name` = ?," +
                        " `description` = ?" +
                        " WHERE `id` = ?" +
                        " LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, description);
        statement.setString(3, id);
        statement.execute();
        statement.close();
    }

    public void updateEntityStatusById(
            @Nullable String userId,
            @Nullable Status status, @Nullable String id) throws SQLException {
        if (status == null) return;
        @NotNull final String query =
                "UPDATE " + getTableName() +
                        " SET `status` = ?" +
                        " WHERE `id` = ?" +
                        " LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, status.toString());
        statement.setString(2, id);
        statement.execute();
        statement.close();
    }

}
