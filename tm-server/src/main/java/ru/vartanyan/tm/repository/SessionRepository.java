package ru.vartanyan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.ISessionRepository;
import ru.vartanyan.tm.model.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(Connection connection) {
        super(connection);
    }

    @Override
    protected String getTableName() {
        return "APP_SESSION";
    }

    @Override
    protected Session fetch(ResultSet resultSet) throws SQLException {
        if (resultSet == null) return null;
        @NotNull Session session = new Session();
        session.setId(resultSet.getString("ID"));
        session.setUserId(resultSet.getString("USER_ID"));
        session.setSignature(resultSet.getString("SIGNATURE"));
        session.setTimestamp(resultSet.getLong("TIMESTAMP"));
        return session;
    }

    @Override
    public void add(@Nullable Session session) throws Exception {
        if (session == null) return;
        @NotNull final String query =
                "INSERT INTO `app_session`(`id`, `user_id`, `signature`, `timestamp`) " +
                        "VALUES(?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, session.getId());
        statement.setString(2, session.getUserId());
        statement.setString(3, session.getSignature());
        statement.setLong(4, session.getTimestamp());
        statement.execute();
    }
}
