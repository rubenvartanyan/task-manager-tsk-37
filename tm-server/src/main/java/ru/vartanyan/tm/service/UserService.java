package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.repository.IUserRepository;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.IUserService;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.exception.empty.*;
import ru.vartanyan.tm.exception.system.NoSuchUserException;
import ru.vartanyan.tm.model.User;
import ru.vartanyan.tm.repository.UserRepository;
import ru.vartanyan.tm.util.HashUtil;

import java.sql.Connection;
import java.sql.SQLException;

public class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull IPropertyService propertyService,
            @NotNull IConnectionService connectionService) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    public IUserRepository getRepository(@NotNull Connection connection) {
        return new UserRepository(connection);
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) throws EmptyLoginException, SQLException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = new UserRepository(connection);
        return userRepository.findByLogin(login);
    }

    @Override
    public void removeByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(connection);
            userRepository.removeByLogin(login);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public User create(@NotNull final String login,
                       @NotNull final String password) throws Exception {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @NotNull User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.md5(password));
        user.setRole(Role.USER);
        add(user);
        return user;
    }

    @Override
    public void create(@NotNull final String login,
                       @NotNull final String password,
                       @NotNull final String email) throws Exception {
        if (email.isEmpty()) throw new EmptyEmailException();
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @NotNull User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.md5(password));
        user.setRole(Role.USER);
        add(user);
    }

    @Override
    public User create(@NotNull String login,
                       @NotNull String password,
                       @Nullable Role role) throws Exception {
        if (role == null) throw new EmptyRoleException();
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @NotNull User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.md5(password));
        user.setRole(role);
        add(user);
        return user;
    }

    @Override
    public void setPassword(@NotNull final String userId,
                            @NotNull final String password) throws Exception {
        if (userId.isEmpty()) throw new EmptyIdException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = findById(userId);
        if (user == null) return;
        @Nullable final String hash = HashUtil.md5(password);
        if (hash == null) return;
        user.setPasswordHash(hash);
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(connection);
            userRepository.setPassword(hash, userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void updateUser(@NotNull final String userId,
                           final @Nullable String firstName,
                           final @Nullable String lastName,
                           final @Nullable String middleName
                           ) throws Exception {
        @Nullable final User user = findById(userId);
        if (user == null) throw new NoSuchUserException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(connection);
            userRepository.updateUser(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void unlockUserByLogin(@Nullable String login) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new NoSuchUserException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(connection);
            userRepository.unlockUser(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void unlockUserById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final User user = findById(id);
        if (user == null) throw new NoSuchUserException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(connection);
            userRepository.unlockUser(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void lockUserByLogin(@Nullable String login) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new NoSuchUserException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(connection);
            userRepository.lockUser(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void lockUserById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final User user = findById(id);
        if (user == null) throw new NoSuchUserException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(connection);
            userRepository.lockUser(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
