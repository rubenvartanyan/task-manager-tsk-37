package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.repository.ISessionRepository;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.ISessionService;
import ru.vartanyan.tm.api.service.ServiceLocator;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.empty.EmptyLoginException;
import ru.vartanyan.tm.exception.system.AccessDeniedException;
import ru.vartanyan.tm.exception.system.NoSuchUserException;
import ru.vartanyan.tm.exception.system.WrongPasswordException;
import ru.vartanyan.tm.exception.system.WrongRoleException;
import ru.vartanyan.tm.model.Session;
import ru.vartanyan.tm.model.User;
import ru.vartanyan.tm.repository.SessionRepository;
import ru.vartanyan.tm.util.HashUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final ServiceLocator serviceLocator;

    public SessionService(
            @NotNull IConnectionService connectionService,
            @NotNull ServiceLocator serviceLocator
    ) {
        super(connectionService);
        this.serviceLocator = serviceLocator;
    }

    public ISessionRepository getRepository(@NotNull Connection connection) {
        return new SessionRepository(connection);
    }

    @Override
    public @Nullable Session open(final String login,
                                  final String password)
            throws Exception {
        final boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        final @Nullable User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return null;
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        @Nullable final Session signSession = sign(session);
        if (signSession == null) return null;
        final Connection connection = connectionService.getConnection();
        try {
            final ISessionRepository sessionRepository = new SessionRepository(connection);
            sessionRepository.add(signSession);
            connection.commit();
            return signSession;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void validate(@Nullable Session session,
                         @Nullable Role role) throws Exception {
        if (role == null) throw new AccessDeniedException();
    }

    @Override
    public void validate(@Nullable Session session) throws AccessDeniedException, EmptyIdException, SQLException {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        final String signatureSource = session.getSignature();
        final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        System.out.println(signatureSource);
        System.out.println(signatureTarget);
        System.out.println(signatureSource);
        System.out.println(signatureTarget);
        if (!check) throw new AccessDeniedException();
        final Connection connection = connectionService.getConnection();
        final ISessionRepository sessionRepository = new SessionRepository(connection);
        if (!sessionRepository.contains(session.getId())) throw new AccessDeniedException();

    }

    @Override
    public void validateAdmin(@Nullable Session session,
                              @Nullable Role role) throws Exception, WrongRoleException {
        if (session == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        if ((session.getUserId()).isEmpty()) throw new AccessDeniedException();
        validate(session);
        final @Nullable User user = serviceLocator.getUserService().findById(session.getUserId());
        if (user == null) throw new AccessDeniedException();
        System.out.println(user.getRole());
        if (user.getRole() != Role.ADMIN) throw new WrongRoleException();
    }

    @Override
    public @Nullable Session close(@Nullable Session session) throws Exception {
        if (session == null) return null;
        final Connection connection = connectionService.getConnection();
        try {
            final ISessionRepository sessionRepository = new SessionRepository(connection);
            sessionRepository.removeById(session.getId());
            connection.commit();
            return session;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public boolean checkDataAccess(@NotNull String login,
                                   @NotNull String password) throws NoSuchUserException, EmptyLoginException, WrongPasswordException, SQLException {
        if (login.isEmpty()) return false;
        if (password.isEmpty()) return false;
        final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new NoSuchUserException();
        final String passwordHash = HashUtil.md5(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        boolean check = passwordHash.equals(user.getPasswordHash());
        if (!check) throw new WrongPasswordException();
        return (true);
    }

    @Override
    public Session sign(Session session) {
        if (session == null) return null;
        session.setSignature(null);
        final IPropertyService propertyService = serviceLocator.getPropertyService();
        final String signature = HashUtil.salt(propertyService, session);
        session.setSignature(signature);
        return session;
    }

}
