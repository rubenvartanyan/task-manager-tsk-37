package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.api.IBusinessRepository;
import ru.vartanyan.tm.api.repository.ITaskRepository;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.ITaskService;
import ru.vartanyan.tm.exception.empty.EmptyDescriptionException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.repository.TaskRepository;

import java.sql.Connection;

public class TaskService extends AbstractBusinessService<Task> implements ITaskService {

    public TaskService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    public IBusinessRepository<Task> getRepository(@NotNull Connection connection) {
        return new TaskRepository(connection);
    }

    @Override
    public void add(@NotNull final String name,
                    @NotNull final String description,
                    @NotNull final String userId) throws Exception {
        if (name.isEmpty()) throw new EmptyNameException();
        if (description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        final Connection connection = connectionService.getConnection();
        try {
            final ITaskRepository taskRepository = new TaskRepository(connection);
            taskRepository.add(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
