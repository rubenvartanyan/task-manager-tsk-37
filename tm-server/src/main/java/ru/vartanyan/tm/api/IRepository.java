package ru.vartanyan.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.model.AbstractEntity;

import java.sql.SQLException;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void add(@NotNull final E entity) throws Exception;

    void remove(@NotNull final E entity) throws Exception;

    @Nullable
    E findById(@NotNull final String id) throws EmptyIdException, SQLException;

    void removeById(@NotNull final String id) throws Exception;

    @Nullable
    List<E> findAll() throws Exception;

    void clear() throws SQLException;

    void addAll(final List<E> entities) throws Exception;

    boolean contains(@NotNull String id) throws SQLException, EmptyIdException;

}
