package ru.vartanyan.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.model.User;

import java.sql.SQLException;

public interface IUserRepository extends IRepository<User>{

    @Nullable
    User findByLogin(@NotNull final String login) throws SQLException;

    void removeByLogin(@NotNull final String login) throws SQLException;

    void setPassword(@NotNull String password, @NotNull String userId) throws SQLException;

    void lockUser(@NotNull User user) throws SQLException;

    void unlockUser(@NotNull User user) throws SQLException;

    void updateUser(@NotNull User user) throws SQLException;

}
