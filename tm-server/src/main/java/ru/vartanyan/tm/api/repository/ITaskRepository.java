package ru.vartanyan.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IBusinessRepository;
import ru.vartanyan.tm.model.Task;

import java.sql.SQLException;
import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    @Nullable
    List<Task> findAllByProjectId(@NotNull final String projectId,
                                            @NotNull final String userId) throws SQLException;

    void removeAllByProjectId(@NotNull final String projectId,
                              @NotNull final String userId) throws SQLException;

    void bindTaskByProjectId(@NotNull final String projectId,
                             @NotNull final String taskId,
                             @NotNull final String userId) throws SQLException;

    void unbindTaskFromProject(@NotNull final String projectId,
                               @NotNull final String taskId,
                               @NotNull final String userId) throws SQLException;

}
