package ru.vartanyan.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.repository.IProjectRepository;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.marker.UnitCategory;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.model.User;
import ru.vartanyan.tm.service.ConnectionService;
import ru.vartanyan.tm.service.PropertyService;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProjectRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    final Connection connection = connectionService.getConnection();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository(connection);

    @After
    @SneakyThrows
    public void after() {
        connection.commit();
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void addAllProjectsTest() {
        final List<Project> projects = new ArrayList<>();
        final Project project1 = new Project();
        final Project project2 = new Project();
        projects.add(project1);
        projects.add(project2);
        projectRepository.addAll(projects);
        Assert.assertNotNull(projectRepository.findById(project1.getId()));
        Assert.assertNotNull(projectRepository.findById(project2.getId()));
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void addProjectTest() {
        final Project project = new Project();
        projectRepository.add(project);
        Assert.assertNotNull(projectRepository.findById(project.getId()));
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void clearProjectsTest() {
        projectRepository.clear();
        Assert.assertTrue(projectRepository.findAll().isEmpty());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void findAllProjects() {
        projectRepository.clear();
        final List<Project> projects = new ArrayList<>();
        final Project project1 = new Project();
        final Project project2 = new Project();
        projects.add(project1);
        projects.add(project2);
        try {
            projectRepository.addAll(projects);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertEquals(2, projectRepository.findAll().size());
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void findProjectByIdTest() {
        final Project project = new Project();
        final String projectId = project.getId();
        projectRepository.add(project);
        Assert.assertNotNull(projectRepository.findById(projectId));
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void findProjectByIndexTest() {
        projectRepository.clear();
        final Project project = new Project();
        final User user = new User();
        final String userId = user.getId();
        project.setUserId(userId);
        projectRepository.add(project);
        final Project project1 = projectRepository.findOneByIndex(0, userId);
        Assert.assertEquals(project.getId(), project1.getId());
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void findProjectByNameTest() {
        final Project project = new Project();
        final User user = new User();
        final String userId = user.getId();
        project.setUserId(userId);
        project.setName("project1");
        projectRepository.add(project);
        final String name = project.getName();
        Assert.assertNotNull(name);
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void removeProjectByIdTest() {
        final Project project1 = new Project();
        projectRepository.add(project1);
        final String projectId = project1.getId();
        projectRepository.removeById(projectId);
        Assert.assertNull(projectRepository.findById(projectId));
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void removeProjectByNameTest() {
        final Project project = new Project();
        final User user = new User();
        final String userId = user.getId();
        project.setUserId(userId);
        project.setName("project1");
        final String name = project.getName();
        projectRepository.add(project);
        projectRepository.removeOneByName(name, userId);
        Assert.assertNull(projectRepository.findOneByName(name, userId));
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void removeProjectByIndexTest() {
        projectRepository.clear();
        final Project project = new Project();
        final String projectId = project.getId();
        final User user = new User();
        final String userId = user.getId();
        project.setUserId(userId);
        project.setName("project1");
        final String name = project.getName();
        projectRepository.add(project);
        projectRepository.removeOneByIndex(0, userId);
        Assert.assertNull(projectRepository.findById(name, projectId));
    }

}
