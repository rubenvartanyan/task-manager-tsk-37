package ru.vartanyan.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.repository.ITaskRepository;
import ru.vartanyan.tm.api.repository.IUserRepository;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.marker.UnitCategory;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.model.User;
import ru.vartanyan.tm.service.ConnectionService;
import ru.vartanyan.tm.service.PropertyService;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

public class TaskRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    final Connection connection = connectionService.getConnection();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository(connection);

    @After
    @SneakyThrows
    public void after() {
        connection.commit();
    }

    @Test
    @Category(UnitCategory.class)
    public void addAllTasksTest() throws Exception {
        final List<Task> tasks = new ArrayList<>();
        final Task task1 = new Task();
        final Task task2 = new Task();
        tasks.add(task1);
        tasks.add(task2);
        taskRepository.addAll(tasks);
        Assert.assertNotNull(taskRepository.findById(task1.getId()));
        Assert.assertNotNull(taskRepository.findById(task2.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void addTaskTest() throws Exception {
        final Task task = new Task();
        taskRepository.add(task);
        Assert.assertNotNull(taskRepository.findById(task.getId()));
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void clearTasksTest() {
        taskRepository.clear();
        Assert.assertTrue(taskRepository.findAll().isEmpty());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void findAllTasks() {
        taskRepository.clear();
        final List<Task> tasks = new ArrayList<>();
        final Task task1 = new Task();
        final Task task2 = new Task();
        tasks.add(task1);
        tasks.add(task2);
        try {
            taskRepository.addAll(tasks);
        } catch (ru.vartanyan.tm.exception.system.NullObjectException e) {
            e.printStackTrace();
        } catch (java.sql.SQLException throwables) {
            throwables.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertEquals(2, taskRepository.findAll().size());
    }

    @Test
    @Category(UnitCategory.class)
    public void findTaskByIdTest() throws Exception {
        taskRepository.clear();
        final Task task = new Task();
        final String taskId = task.getId();
        taskRepository.add(task);
        Assert.assertNotNull(taskRepository.findById(taskId));
    }

    @Test
    @Category(UnitCategory.class)
    public void findTaskByIndexTest() throws Exception {
        taskRepository.clear();
        final Task task = new Task();
        final User user = new User();
        final String userId = user.getId();
        task.setUserId(userId);
        taskRepository.add(task);
        Task task1 = taskRepository.findOneByIndex(0, userId);
        Assert.assertEquals(task.getId(), task1.getId());
    }

    @Test
    @Category(UnitCategory.class)
    public void findTaskByNameTest() throws Exception {
        taskRepository.clear();
        final Task task = new Task();
        final User user = new User();
        final String userId = user.getId();
        task.setUserId(userId);
        task.setName("task1");
        taskRepository.add(task);
        final String name = task.getName();
        Assert.assertNotNull(name);
        Assert.assertEquals(task, taskRepository.findOneByName(name, userId));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTaskByIdTest() throws Exception {
        taskRepository.clear();
        final Task task1 = new Task();
        taskRepository.add(task1);
        final String taskId = task1.getId();
        taskRepository.removeById(taskId);
        Assert.assertNull(taskRepository.findById(taskId));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTaskByNameTest() throws Exception {
        taskRepository.clear();
        final Task task = new Task();
        final User user = new User();
        final String userId = user.getId();
        task.setUserId(userId);
        task.setName("task1");
        final String name = task.getName();
        taskRepository.add(task);
        taskRepository.removeOneByName(name, userId);
        Assert.assertNull(taskRepository.findOneByName(name, userId));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTaskByIndexTest() throws Exception {
        taskRepository.clear();
        final Task task = new Task();
        final String taskId = task.getId();
        final User user = new User();
        final String userId = user.getId();
        task.setUserId(userId);
        task.setName("task1");
        final String name = task.getName();
        taskRepository.add(task);
        taskRepository.removeOneByIndex(0, userId);
        Assert.assertNull(taskRepository.findById(name, taskId));
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void findAllTasksByProjectIdTest() {
        taskRepository.clear();
        final User user = new User();
        final String userId = user.getId();
        final Project project = new Project();
        final Task task1 = new Task();
        final Task task2 = new Task();
        final String projectId = project.getId();
        task1.setUserId(userId);
        task2.setUserId(userId);
        task1.setProjectId(projectId);
        task2.setProjectId(projectId);
        try {
            taskRepository.add(task1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            taskRepository.add(task2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        final List<Task> taskList = taskRepository.findAllByProjectId(projectId, userId);
        Assert.assertEquals(2, taskList.size());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void removeAllByProjectIdTest() {
        taskRepository.clear();
        final User user = new User();
        final String userId = user.getId();
        Project project = new Project();
        Task task1 = new Task();
        task1.setProjectId(project.getId());
        task1.setUserId(userId);
        Task task2 = new Task();
        task2.setProjectId(project.getId());
        task2.setUserId(userId);
        taskRepository.removeAllByProjectId(project.getId(), userId);
        final long size = taskRepository.findAllByProjectId(project.getId(), userId).size();
        Assert.assertEquals(0, size);
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void bindTaskByProjectIdTest() {
        taskRepository.clear();
        final User user = new User();
        UserRepository userRepository = new UserRepository(connection);
        userRepository.add(user);
        final String userId = user.getId();
        final Project project = new Project();
        ProjectRepository projectRepository = new ProjectRepository(connection);
        project.setUserId(userId);
        projectRepository.add(project);
        final String projectId = project.getId();
        final Task task = new Task();
        task.setUserId(userId);
        taskRepository.add(task);
        final String taskId = task.getId();
        taskRepository.bindTaskByProjectId(projectId, taskId, userId);
        Assert.assertEquals(taskRepository.findById(taskId, userId).getProjectId(), projectId);
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void unbindTaskFromProjectTest() {
        taskRepository.clear();
        final User user = new User();
        final String userId = user.getId();
        final Project project = new Project();
        project.setUserId(userId);
        final String projectId = project.getId();
        final Task task = new Task();
        task.setUserId(userId);
        final String taskId = task.getId();
        try {
            taskRepository.add(task);
        } catch (Exception e) {
            e.printStackTrace();
        }
        taskRepository.bindTaskByProjectId(projectId, taskId, userId);
        List<Task> taskList = new ArrayList<>();
        taskList.add(task);
        taskRepository.unbindTaskFromProject(projectId, taskId, userId);
        Assert.assertNull(task.getProjectId());
    }

}
